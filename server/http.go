package webfinger

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/bakyun/jrd"
)

// WebFingerPath defines the default path of the webfinger handler.
const WebFingerPath = "/.well-known/webfinger"

// ServeHTTP serves the webfinger request
func (s *Service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.runPrehandlers(w, r)

	if r.TLS == nil && !s.AllowHTTP {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	query := r.URL.Query()
	resourceList := query["resource"]
	if len(resourceList) != 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	resource := resourceList[0]
	uri, err := url.Parse(resource)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	rels := query["rel"]
	var jrd *jrd.JRD
	if uri.Scheme == "acct" {
		segments := strings.Split(uri.Opaque, "@")
		if len(segments) != 2 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		jrd, err = s.Resolver.FindUser(segments[0], segments[1], rels)
	} else {
		jrd, err = s.Resolver.FindURI(uri, rels)
	}

	switch {
	case err == ErrNotFound:
		w.WriteHeader(http.StatusNotFound)
		return
	case err != nil:
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := json.NewEncoder(w).Encode(&jrd); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (s *Service) runPrehandlers(w http.ResponseWriter, r *http.Request) {
	if s.PreHandlers == nil {
		return
	}

	for _, val := range s.PreHandlers {
		if val != nil {
			val.ServeHTTP(w, r)
		}
	}
}
