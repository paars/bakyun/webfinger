module gitlab.com/bakyun/webfinger

go 1.12

require (
	github.com/ant0ine/go-webfinger v0.0.0-20150209052316-f8a1773b0e03
	gitlab.com/bakyun/jrd v0.0.0-20190215064341-153427b2ab83
)
